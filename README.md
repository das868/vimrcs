# vimrcs
This repositroy started off as a collection of my old and new ~/.vimrc files, capable of being used as a fallback mode in case the pathogen uninstall and Vundle install should fail.

Luckily, everything went OK so that this repository is now supposed to be a source of inspiration for others who are relatively new to Vim and begin to set up their very first `~/.vimrc` file.

Make sure your system has the full `vim` instead of `vim-tiny` installed; you can check by running `vim --version` in your terminal.

You can install any of these configurations by running

    git clone https://gitlab.com/das868/vimrcs

inside the destination directory and then copying the desired dotfile to your home directory.
For Vundle installation Instructions, see [VundleVim](https://github.com/VundleVim/Vundle.vim).

**UPDATE:**
Currently switching from Vim 7.4 to the new 8.0 so expect this repo to be reorganized soon...

Feel free to clone and distribute and if you’re new to Vim, never give up (or start using Emacs :-) )!

Availible under the GNU General Public License v.3

Check out [Strahlenschutzkommando](https://github.com/Strahlenschutzkommando)!
