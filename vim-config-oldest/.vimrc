" echo "MY VIMRC LOADED"
set number

execute pathogen#infect()
" set background=light
" colorscheme solarized
colo badwolf

set ruler
set nocompatible
syntax on
filetype plugin on
filetype indent on
filetype on
set clipboard=unnamed
" set background=dark
" set cursorline
" set colorcolumn=80

" set autoindent
set showmode
set showcmd
set relativenumber
" set wildmenu
set incsearch
set hlsearch
set showmatch

set tabstop=2
set wildmenu
set path+=**
set laststatus=2
