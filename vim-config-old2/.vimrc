" ---JUST FOR FORMER TESTING PURPOSES:---
" echo "MY VIMRC LOADED"





" B R E A K I N G   H A B I T S, M A K I N G   H A B I T S !!!
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>





" ---VUNDLE REQUIREMENTS AND CONFIG---
" sei Vi IMproved, nicht Dein Urgroßvater!, nötig für Vundle
set nocompatible
filetype off					" nötig für Vundle

" Vundle in runtime path miteinbeziehen
set rtp+=~/.vim/bundle/Vundle.vim 

" Vundle initialisieren
call vundle#begin()	



" let Vundle manage Vundle, nötig
Plugin 'VundleVim/Vundle.vim' 		

" badwolf theme installieren
Plugin 'sjl/badwolf'	

" SimplyFold für besseres Code Folding
" !!!!!GIBTS NICHT MEHR!!!!!
" Plugin 'tmhedberg/SimplyFold'

" indentpython für PEP8-konforme Einrückungen und -support
Plugin 'vim-scripts/indentpython.vim'

" auto-complete für Python
Bundle 'Valloric/YouCompleteMe'

" syntax check (for Python or even all languages?)
Plugin 'scrooloose/syntastic'

" PEP8 checking for Python
Plugin 'nvie/vim-flake8'

" file browser ~ file tree ~ NERDTree
Plugin 'scrooloose/nerdtree'
" tabs for NERDTree
Plugin 'jistr/vim-nerdtree-tabs'

" Powerline [!] – NOT YET BEAUTIFUL...
" Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

" Vundle-Initialisierung stoppen, nötig
call vundle#end()			

" nötig für Vundle und schön!
filetype plugin indent on





" ---MY PERSONAL FLAVOR CONFIG---
"  --BASIC AND  STANDARD STUFF--
set number

" set background=light
" colorscheme solarized
colo badwolf

set ruler
" SCHON DA: set nocompatible
syntax on
" DAS GEHT KÜRZER, S.O. filetype plugin on
" DAS GEHT KÜRZER, S.O. filetype indent on
" DAS GEHT NICHT MEHR, S.O. filetype on
set clipboard=unnamed
set encoding=utf-8
" set background=dark
" set cursorline
" set colorcolumn=80

" set autoindent
set showmode
set showcmd
set relativenumber
" set wildmenu
set incsearch
" GEHT MIR AUF DEN ZEIGER: set hlsearch
set showmatch

" MORE DETAILED BELOW: set tabstop=2
set wildmenu
set path+=**
set laststatus=2





" --PYTHON IDE STUFF FROM
"  https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/
"  -- 

" make Python code look more pretty
let python_highlight_all=1

" specify different areas for splitting
set splitbelow
set splitright

" split navigations
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

" Enable code folding
set foldmethod=indent
set foldlevel=99

" Enable folding with spacebar
" For better folding use Plugin SimplyFold, S.O.
nnoremap <space> za

" Enable docstring preview for folded code
" !!!!!GEHT NICHT MEHR!!!!!
" let g:SimplyFold_docstring_preview=1

"  --PYTHON IDENTATION--
" FOR PEP8 CONFORMITY, CF. INDENTPYTHON.VIM, S.O.
au BufNewFile,BufRead *.py
	\ set tabstop=4
	\ set softtabstop=4
	\ set shiftwidth=4
	\ set textwidth=79
	\ set expandtab
	\ set autoindent
	\ set fileformat=unix

" --IDENTATION ELSEWHERE--
au BufNewFile,BufRead *.js, *.html, *.css
	\ set tabstop=2
	\ set softtabstop=2
	\ set shiftwidth=2
	"\ set autoindent

au BufNewFile,BufRead *.c, *.cpp, *.java
	\ set tabstop=4
	\ set softtabstop=4
	\ set shiftwidth=4
	\ set textwidth=79
	\ set expandtab
	\ set autoindent
	\ set fileformat=unix

" --AUTO-COMPLETE FOR PYTHON VIA PLUGIN YouCompleteMe--
"  -CUSTOMIZATION-
"   close autocomplete window after completion
let g:ycm_autoclose_preview_window_after_completion=1
"   remap leader+g to Go-to-definition-of-code
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>


" DOESN’T WORK: python with virtualenv support
"py << EOF
"import os
"import sys
"if 'VIRTUAL_ENV' in os.environ:
"  project_base_dir = os.environ['VIRTUAL_ENV']
"    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"      execfile(activate_this, dict(__file__=activate_this))
"      EOF


" Hide .pyc files
let NERDTreeIgnore=['\.pyc$', '\~$'] 

" TRY OUT SUPERSEARCH ACROSS MULTIPLE FILES SOME TIME!
