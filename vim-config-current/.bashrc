# OLD:
# export PATH=\$HOME/Downloads/Exercism\ Stuff/:$PATH

#--------------------------------------------------------------------------
#~~~~~~~~~~~~~~~~~~~~~~~MANUALLY INSERTED STUFF~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#--------------------------------------------------------------------------

# Alias declarations
alias alisa='alias' 			#just for fun
alias lg='ls -halF'			#ls g’scheidt
alias lsc='ls --color=auto'		#ls mit color
alias lgc='ls -half --color=auto'	#ls g’scheidt mit color
alias lcg='lgc'				#ignore typos in lgc
alias schr='cd ~/Schreibtisch'

# Shorten file paths 
PROMPT_DIRTRIM=1

# NUR ZUR SICHERHEIT: DAS GLOBALE REMAPPING VON ESC ZU CAPS UND UMGEKEHRT
# 		      FINDET IN /usr/share/X11/xkb/pc STATT; DIE ALTE 
# 		      CONFIG HEIẞT JETZT pc-origi
